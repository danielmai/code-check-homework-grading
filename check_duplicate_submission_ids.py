#!/usr/bin/env python
"""check_duplicate_submission_ids.py

This script checks the for duplicate submission ids.
"""

from __future__ import print_function
import sys
import os
import re
from collections import defaultdict
import argparse

__author__ = "Daniel Mai <daniel.mai01@sjsu.edu>"
__version__ = "0.1.0"

def script_path():
    """Returns the absolute path of this script"""
    pathname = os.path.dirname(sys.argv[0])
    return os.path.abspath(pathname)

def map_submission_ids_to_student_names(directory):
    """Returns a dictionary of the submission ids to student names."""

    def get_submission_id(report_text):
        """Gets the submission id from the report file"""
        sub_pat = r'<meta name="Submission" content="(\d+)"/>'
        match = re.search(sub_pat, report_text)
        if match:
            return match.group(1)  # the submission id

    def get_student_name(s):
        """Gets the student name from the string s
        formatted lastname_firstname_miscname_canvasid"""
        MIN_NUMBER_OF_DIGITS = 5
        pattern = "(^[a-zA-Z_-]+)_\d{%d,}" % MIN_NUMBER_OF_DIGITS
        match = re.search(pattern, s)
        if match:
            return match.group(1)

    sid2name = defaultdict(list) # a dict from submission ids to a
                                  # list of names

    # go through submission files
    for root, dirs, files in os.walk(directory):
        if 'report.html' in files:
            with open('{}/report.html'.format(root)) as f:
                # the codecheck report text
                report_text = f.read()

                # get the student's name
                current_directory = root[root.rindex(os.sep) + 1:]
                name = get_student_name(current_directory)

                # get the submission id from the codecheck report
                sub_id = get_submission_id(report_text)

                sid2name[sub_id].append(name)

    return sid2name

def parse_args():
    """Get command-line arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument('directory',
                        help=('the directory containing subdirectories of '
                              'student submissions.'))
    return parser.parse_args()

def main():
    """The main function."""
    args = parse_args()
    sid2name = map_submission_ids_to_student_names(args.directory)

    duplicates = {k:v for k,v in sid2name.items() if len(v) > 1}

    if not duplicates:
        print("There are no duplicate submission ids.")
    else:
        print("Here are the students who have the same submission id:")
        for submission_id, names in duplicates.items():
            print("Submission id: {}".format(submission_id))
            print("\tStudents:")
            for name in names:
                print("\t{}".format(name))

if __name__ == "__main__":
    sys.exit(main())
