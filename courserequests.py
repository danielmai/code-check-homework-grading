from packages import requests

"""
courserequests

Make HTTP requests to Canvas Instructure through Canvas's API.

Method names are exact slugs of the corresponding titles on the API
found at https://canvas.instructure.com/doc/api/
"""
class CourseRequests(object):

    def __init__(self, course, host, access_token):
        self.course = course
        self.host = host
        self.access_token = access_token
        self.headers = {'Authorization': 'Bearer ' + access_token}
        self.protocol = 'https'

    def get(self, url):
        return requests.get(url, headers=self.headers)

    def check_authentication(self):
        path = '/api/v1/courses'

        url = self.protocol + '://' + self.host + path
        return requests.get(url, headers=self.headers)

    def create_assignment(self, name, points_possible,
                          submission_types=['none'], allowed_extensions=[]):
        path = '/api/v1/courses/{course_id}/assignments'.format(
                   course_id=self.course.id
               )

        params = {
            'assignment[name]': name,
            'assignment[points_possible]': points_possible,
            'assignment[submission_types]': submission_types,
            'assignment[allowed_extensions]': allowed_extensions
            }

        url = self.protocol + '://' + self.host + path
        return requests.post(url, headers=self.headers, params=params)

    def create_conversation(self, recipient_ids, message):
        path = '/api/v1/conversations'

        params = {
            'recipients[]': recipient_ids,
            'body': message
            }

        url = self.protocol + '://' + self.host + path
        return requests.post(url, headers=self.headers, params=params)

    def list_users_in_course(self, enrollment_role='', include=''):
        path = '/api/v1/courses/%s/users' % self.course.id
        params = {
            'include[]': [],
            'per_page': 50 # max results for pagination
        }
        if enrollment_role:
            params['enrollment_role'] = enrollment_role
        if include:
            params['include[]'].append(include)

        url = self.protocol + '://' + self.host + path
        return requests.get(url, headers=self.headers, params=params)

    def grade_or_comment_on_a_submission(self, assignment_id, user_id,
                                         comment='', grade=''):
        path = ('/api/v1/courses/{course_id}'
                '/assignments/{assignment_id}/submissions/{user_id}')
        path = path.format(
            course_id = self.course.id,
            assignment_id = assignment_id,
            user_id = user_id
            )
        params = {
            'comment[text_comment]': comment,
            'submission[posted_grade]': grade
            }

        url = self.protocol + '://' + self.host + path
        return requests.put(url, headers=self.headers, params=params)

    def search_users(self, search_term, include=['email']):
        path = '/api/v1/courses/%s/users' % self.course.id

        params = {
                'search_term': search_term,
                'include[]': include
                }

        url = self.protocol + '://' + self.host + path
        return requests.get(url, headers=self.headers, params=params)

    def list_enrollments(self, type_param=[]):
        # GET /api/v1/courses/:course_id/enrollments
        path = '/api/v1/courses/{course_id}/enrollments'
        path = path.format(course_id=self.course.id)

        params = {'per_page': 50, # max pagination
                  'type': type_param}
        url = self.protocol + '://' + self.host + path
        return requests.get(url, headers=self.headers, params=params)

    def get_assignment_id(self, assignment_name):
        r = self.list_assignments()

        # if the json is a dict, then an error occurred
        if type(r.json()) is dict:
            if 'status' in r.json().keys():
                print(r.json()['status'])
                print(r.json()['message'])
                return None
        else: # else, the json is <type 'list'>
            for assignment in r.json():
                if assignment['name'] == assignment_name:
                    return assignment['id']

    def get_assignment_max_score(self, assignment_name):
        r = self.list_assignments()
        for assignment in r.json():
            if assignment['name'] == assignment_name:
                return assignment['points_possible']

    def list_all_assignments(self):
        r = self.list_assignments()
        all_assignments = r.json()
        while r.links.get('next'):
            r = self.get(r.links['next']['url'])
            all_assignments += r.json()
        return all_assignments

    def list_all_enrollments(self):
        r = self.list_enrollments()
        all_enrollments = r.json()
        while r.links.get('next'):
            r = self.get(r.links['next']['url'])
            all_enrollments += r.json()
        return all_enrollments

    def list_assignments(self):
        path = '/api/v1/courses/%s/assignments' % self.course.id
        params = {
            'include[]': 'submission',
            'per_page': 50
            }
        url = self.protocol + '://' + self.host + path
        return requests.get(url,
                            headers=self.headers, params=params)
 
    def list_assignment_submissions(self, assignment_id):
        path = '/api/v1/courses/%s/assignments/%s/submissions' % (
            self.course.id, assignment_id
            )

        params = {
            'include[]': 'assignment'
            }

        url = self.protocol + '://' + self.host + path
        return requests.get(url, headers=self.headers, params=params)

    def list_available_reports(self, account_id):
        path = '/api/v1/accounts/%s/reports' % account_id

        url = self.protocol + '://' + self.host + path
        return requests.get(url, headers=self.headers)

    def list_accounts(self):
        path = '/api/v1/accounts'

        url = self.protocol + '://' + self.host + path
        return requests.get(url, headers=self.headers)

    def get_course_level_student_summary_data(self):
        path = '/api/v1/courses/%s/analytics/student_summaries' % self.course.id

        url = self.protocol + '://' + self.host + path
        return requests.get(url, headers=self.headers)

    def __str__(self):
        return 'CourseRequests=[course=%s,host=%s,access_token=%s]' % (
            self.course, self.host, self.access_token
        )
