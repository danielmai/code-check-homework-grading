import argparse, os, re, webbrowser, sys, time

try:
    # For Python 3
    import urllib.parse as urllibparse
    import urllib.request as urllibrequest
except ImportError:
    # For Python 2.7.*
    import urlparse as urllibparse
    import urllib as urllibrequest


parser = argparse.ArgumentParser()
parser.add_argument('directory', help='the hw directory to search')
parser.add_argument('key_string', help='the string to search for')
parser.add_argument('-o', '--open',
                    help='open matching files in browser',
                    action='store_true')
                    
args = parser.parse_args()

key_string = args.key_string
directory = args.directory
if directory[-1] != "/":
    directory += "/"
    
matching_files = []
directory_files = os.listdir(directory)
directory_files.sort()
match_found = False
for folder in directory_files:
    if folder == "grades.txt":
        continue
    for file in os.listdir(directory + folder):
        if file == "aggregate_report.html":
            for line in open(directory + folder + "/" + file, "r"):
                if key_string in line:
                    match_found = True
                    if args.open:
                        webbrowser.open(directory + folder + "/" + file)
                        print("opening: " + folder)
                    else:
                        print(folder)
                    break

if not match_found:
    print("no matches found")
