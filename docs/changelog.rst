===========
 Changelog
===========

- v2.2.9 (2016-02-21)

  - Removes student name from kudos comment (e.g., "Good job, NAME" or
    "Well done, NAME", because the script was actually writing out
    "Good job, None" and "Well done, None" (Canvas's zip format
    changed. See BitBucket Issue #3).
  - Change DEBUG level log format to have the filename, line number,
    function name, and message.

- v2.2.8 (2015-07-01)

  - Check **all** assignments when searching for one by name, not just
    the first 50 results. (The changes in v2.2.3 was not good enough.)

- v2.2.7 (2015-03-16)

  - Before uploading grades, check if the assignment exists first.

- v2.2.6 (2015-02-06)

  - Ignore 404s when uploading grades. The previous behavior was for
    the script to fail-fast when there is an assignment ID error.
    However, this also makes the script crash when the student canvas
    ID isn't found in the API, usually because of a student dropping
    the course.

- v2.2.5 (2014-10-12)

  - Change the way the score is searched in the grading file comments.
    The scores can only happen at the beginning of a line, not
    anywhere in the line (this unintentionally made multiple score
    changes made to a submission).

- v2.2.4 (2014-10-10)

  - Fix an issue with the codecheck files not being verified correctly
    by the keystore file.

- v2.2.3 (2014-10-07)

  - list_assignments returns up to 50 assignments instead of the
    default 10.

- v2.2.2 (2014-09-06)

  - Add public key, ``codecheck-public.jks`` to handle the codecheck
    files from the new server (cs19).

- v2.2.1 (2014-09-05)

  - Bug fix: UnicodeDecodeError when reading from java files.
    Solution: Remove file reading encoding when reading java files.

- v2.2.0 (2014-02-23)

  - Add the ``check_duplicate_submission_ids.py`` script to check for
    duplicate submission ids within an assignment.
