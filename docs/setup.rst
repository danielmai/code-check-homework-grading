Initial setup
-------------

Python
~~~~~~

Python needs to be installed on your system. You can download Python
at the `official download page <http://www.python.org/download/>`__.
They should be compatible with any Python 2.7.x and Python 3.x
versions. The scripts were created using Python 2.7.5. I've tested
these scripts with Python 3.3 as well, and they seemed to work just
fine.

.. _canvas-access-token:

JDK tools
~~~~~~~~~

You need to have the JDK installed and the command-line tools on your
path, because the scripts use the ``jar`` and ``jarsigner`` commands.
On Windows, refer to `instructions at Oracle's documentation <http://docs.oracle.com/javase/7/docs/webnotes/install/windows/jdk-installation-windows.html#path>`__ to set up
your command-line path to have the Java tools ready. On Linux and OS
X, the tools should automatically be found on your path.

Canvas access token
~~~~~~~~~~~~~~~~~~~

**You must create a Canvas access token** in order to grade
assignments. This allows you to submit grades and comments for
homework under your account (so long as you have permission to, which
you should have because you're a grader). An access token can be
created at the bottom of your `Canvas settings
<https://sjsu.instructure.com/profile/settings>`__ and clicking on the
blue "+ New Access Token" button, which is shown below.

.. figure:: user-settings-access-token.png
   :alt: Canvas settings access token section

   Above: The access token section of the Canvas setting page
   
.. note:: An access token is like a password, so keep it away from others, as it
	  allows anyone to use the Canvas API under your account. You only need
	  one access token for all of your classes because the token is for
	  your whole account, not tied to any particular account. So if you're
	  grading for multiple courses with this script, you can use the same
	  access token for all of them.
   
Download the scripts
~~~~~~~~~~~~~~~~~~~~

You can download the scripts by cloning the repository with the
following command:

::

    git clone https://bitbucket.org/danielmai/code-check-homework-grading.git

By cloning the repository, it will be easier to get the latest
versions of the scripts with a ``git pull``. Remember to pull the
latest scripts from the repository before grading.

Modify ``config.json``
~~~~~~~~~~~~~~~~~~~~~~

There is a ``config.json`` file that contains information necessary to
make calls to the Canvas API. It should contain the correct
information for the following fields:

-  ``access_token``

   -  The API access token for Canvas. To create one for yourself, see
      :ref:`the Canvas access token section (above) <canvas-access-token>`.

-  ``course_id``

   -  A string of digits representing the Canvas course ID. It can be
      found from the URL of the course page on Canvas (the only string
      of digits in the URL).

   .. note:: Make sure you have the correct course ID for the course
	     you are grading for **this semester**. If you used this
	     script for a previous course, you may have kept the same
	     information relevant to your previous course.

	     If you have the wrong information in your config file,
	     the script may appear to be working just fine, but bad
	     things **will** happen (like uploading the grades with
	     the same assignment name *to the wrong course*), so please
	     double check this.

-  ``course_name``

   -  (Optional) This is used as a label in the config file so you know
      what course the config information is pertaining to.

-  ``host``

   -  The host for Canvas. This will be specific to the university's
      Canvas installation. SJSU Canvas's host is
      ``sjsu.instructure.com``.


You can modify the config.json file directly, or by using
``grade_config.py`` (but modifying it directly is probably the way to
go---it's just a JSON file). If you want to use the provided
grade_config.py script, use ``grade_config.py --help`` for instructions.

If you are a past grader, or grading for multiple classes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There is only one ``config.json`` file that the script will read from,
so you must make sure that the correct information is in your
configuration file (if that didn't become apparent in the note above).

If you are grading multiple classes at the same time, then you should
make a copy of the config file and manage them yourself. When you use
the script, make sure you are using the correct config file.
