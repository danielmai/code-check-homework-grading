Development
-----------

This program is written and tested on a computer running Mac OS X 10.9
running Python 2.7.5 and Python 3.3.3.

`Canvas API documentation <https://canvas.instructure.com/doc/api/>`__
was used to figure out what API calls to make in this script.

This script makes HTTP requests to Canvas's API with the help of
Python's Requests library (it has made development easier and the code
cleaner). More information and documentation on the library can be found
`here <http://docs.python-requests.org/>`__.

Dependencies
~~~~~~~~~~~~

-  `Requests <http://docs.python-requests.org/>`__ (used in terms with
   the `Apache License, Version
   2.0 <http://www.apache.org/licenses/LICENSE-2.0>`__)
-  Java JDK tools (``jar`` and ``jarsigner``)
-  `Canvas API <https://canvas.instructure.com/doc/api/>`__
-  Canvas submission zip file structure
-  codecheck report structure
-  Python and its standard library

Support
~~~~~~~

If you have any questions or problems with the scripts, please email me
at daniel.mai01@sjsu.edu.
