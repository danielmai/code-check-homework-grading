"""
course

Defines a course from Canvas, which has a name and an ID.
"""

class Course(object):

    def __init__(self, name, course_id):
        self.name = name
        self.id = course_id

    def __str__(self):
        return 'Course=[name=%s,course_id=%s]' % (self.name, self.id)
