# first test of pyramid language

import argparse

if __name__ == '__main__':

    # Adding verbose option
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', help="verbose output.", action="store_true")
    args = parser.parse_args()

    # initial python test
    if args.verbose:
        print("Debugging mode on!")



