#!/usr/bin/env python

"""
grade_upload

Uses the grades.txt file that the grader has used to grade the assignment
and uploads the students' grades to Canvas.

Make sure that the correct grades.txt file is uploaded.
"""

import argparse
import json
import os
import re
import sys

from packages import requests

from course import Course
from courserequests import CourseRequests

# grades.txt keys
# Python 2 compatibility
if sys.version[0] == "2": input = raw_input

# grades.txt keys - these keys will appear in the grades.txt file
ASSIGNMENT_NAME_KEY = "_canvas_assignment_name" # the Canvas assignment name
ASSIGNMENT_ID_KEY = "_canvas_assignment_id" # the Canvas assignment id

DIR_KEY = "_dir" # the name of the directory with the student's submission
NAME_KEY = "_name" # the name of the student
ID_KEY = "_canvas_id" # the Canvas ID of the student
SCORE_KEY = "_total_score" # the score the student received for the assignment
COMMENT_KEY = "_comments" # comments for a student's submission on Canvas and score modifications
EXTRA_KEY = "_notes" # notes and score modifications

SEPARATOR = '-' * 8

def get_grades_dict(grades_file_path):
    """
    Returns a dictionary of assignment keys for each student
    canvas_id -> {
                   "name": student_name,
                   "score": score (numerator only),
                   "comment": comment,
                   "extra": comment
                 }
    """
    def clean_up_data(section, start, end):
        """
        Returns the information delimited by the
        keys `start` and `end` for the section
        """
        s = section # to make code more succinct
        # Need the [1:] slice to get rid of the ':' character
        return s[s.find(start) + len(start):s.find(end)][1:].strip()

    def get_points_received(score):
        """
        Returns the numerator of the score as an int.
        The score is a string in the format "xxx/yyy"
        """
        return int(score[:score.find('/')])

    def change_score_if_needed(point_changes, score):
        """
        Given the extra section combined with the comment section and the score, change the score as
        indicated by the grader comments.

        Returns the modified score if it needed to be modified; else
        return the original score
        """
        lines = point_changes.split('\n')

        for line in lines:
            tokens = line.split(' ')

            possible_score = tokens[0] # 'possible'--it may not be a number
            score_format = r'[+-]([0-9]*[.])?[0-9]+' # Old versions that only worked with int's: r'[+-]\d+'
            match = re.match(score_format, possible_score)
            if match:
                score_change = float(match.group()) # Old versions that only worked with int's: int(match.group())
                score = score + score_change
        return score


    canvas_id_to_data = {}

    with open(grades_file_path, 'r' ) as grades_file:
        content = grades_file.read().split(SEPARATOR)
        del(content[-1]) # delete last "\n"

        sections = iter(content) # make it an iterable in order to consume the
                                 # header with the assignment name and id,
                                 # while keeping the bottom loop untouched

        if content[0].startswith(ASSIGNMENT_NAME_KEY):
            header = next(sections)

            global assignment_name
            global assignment_id
            assignment_name = clean_up_data(header, ASSIGNMENT_NAME_KEY,
                                            ASSIGNMENT_ID_KEY)
            assignment_id = clean_up_data(header, ASSIGNMENT_ID_KEY, SEPARATOR)

        for section in sections:
            # all string types
            name      = clean_up_data(section, NAME_KEY, ID_KEY)
            canvas_id = clean_up_data(section, ID_KEY, SCORE_KEY)
            score     = clean_up_data(section, SCORE_KEY, COMMENT_KEY)
            comment   = clean_up_data(section, COMMENT_KEY, EXTRA_KEY)
            extra     = clean_up_data(section, EXTRA_KEY, SEPARATOR)

            score = get_points_received(score) # int

            # Applying score changes
            score = change_score_if_needed(comment, score)

            canvas_id_to_data[canvas_id] = {
                'name': name,
                'score': score,
                'comment': comment,
                'extra': extra
                }

    return canvas_id_to_data

def script_path():
    """Returns the absolute path of this script"""
    pathname = os.path.dirname(sys.argv[0])
    return os.path.abspath(pathname)

def upload_grades(submitter_grades):
    """
    Uploads the grades for all students in the course

    Students who did not submit anything (and thus are not
    in the grades.txt file) will receive a zero.

    submitter_grades is the dictionary in the same format that is
    returned by get_grades_dict()
    """

    # --------------- helper methods ---------------
    def upload_grades_to_canvas(grades):
        """Uploads grades to Canvas. Grades is a dictionary of
        Canvas IDs to scores and comments """
        for canvas_id, submission_info in grades.items():
            if args.no_comments:
                r = cr.grade_or_comment_on_a_submission(assignment_id,
                                                    canvas_id,
                                                    "",
                                                    submission_info['score'])
            else:
                r = cr.grade_or_comment_on_a_submission(assignment_id,
                                                    canvas_id,
                                                    submission_info['comment'],
                                                    submission_info['score'])

            if args.verbose:
                # Print out the the upload data
                print(  str(canvas_id) + ": " + str(submission_info['score']) + "\n" +
                        str(submission_info['comment']) + "\n\n")




    def get_nonsubmitter_grades():
        """Gets grades for those who did not turn in the assignment."""
        # get students
        # a student JSON object looks like
        #    {  u'id': 123567,
        #       u'name': u'First Last',
        #       u'short_name': u'First Last (Possibly a nickname)',
        #       u'sortable_name': u'Last, First'},
        r = cr.list_users_in_course(enrollment_role='StudentEnrollment',
                                    include='')
        all_students = r.json()

        # get all of the students from paginated results
        while r.links.get('next'):
            r = cr.get(r.links['next']['url'])
            all_students += r.json()

        all_student_ids = set(str(student['id']) for student in all_students)

        non_submission_ids = [x for x in all_student_ids if x not in submitter_grades]
        comment_score = {
            'comment': 'No submission.',
            'score': 0
            }
        grades = {s_id:comment_score for s_id in non_submission_ids}
        return grades

    # --------------- end helper methods ---------------

    upload_grades_to_canvas(submitter_grades)
    upload_grades_to_canvas(get_nonsubmitter_grades())


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', help="verbose output.",
                        action="store_true")
    parser.add_argument('-n', '--no-comments',
                        help='upload only the grades, without the comments.',
                        action='store_true')
    parser.add_argument('-a', '--assignment-name',
                          help=('the assignment name. '
                                'Must match an assignment on Canvas. '
                                'By supplying this option, it will override '
                                'the assignment name found in the '
                                'the grades.txt file (if it exists) and '
                                'and will use the provided name to search '
                                'for the id.'))
    parser.add_argument('grades_txt',
                        help='the grades.txt file for the assignment')

    args = parser.parse_args()

    # Canvas config info
    canvas_keys = {}
    with open('%s/config.json' % script_path()) as config_file:
        canvas_keys = json.loads(config_file.read())

    # Creates the objects for the Canvas course information
    course = Course(canvas_keys['course_name'], canvas_keys['course_id'])
    cr = CourseRequests(course, canvas_keys['host'],
                        canvas_keys['access_token'])

    # Gets the information of the grades.txt file as a dictionary
    grades_info = get_grades_dict(args.grades_txt)

    if args.assignment_name:
        assignment_name = args.assignment_name
        print("Getting %s's ID..." % assignment_name)
        assignment_id = cr.get_assignment_id(assignment_name)

    if not assignment_id:
        sys.exit(("Error: There's no ID for '%s'.\nMake sure "
                  "the assignment name matches exactly "
                  "what's on Canvas.") % assignment_name)

    # Check if the assignment exists
    r = cr.list_assignment_submissions(assignment_id)
    if r.status_code != requests.codes.ok:
        sys.exit("Error: Assignment ID '%s' does not exist." % assignment_id)

    print("%s's ID is %s" % (assignment_name, assignment_id))

    print("Checking your authentication key...")
    r = cr.check_authentication()
    r.raise_for_status()
    print('OK.') # if code runs at this point, then status is OK.

    if args.no_comments:
        comments_string = 'WITHOUT comments'
    else:
        comments_string = 'with comments'

    ready_to_submit = input("> Ready to submit grades %s (y/n)? " % (
        comments_string
        )
                               )
    if ready_to_submit.lower() == 'y':
        upload_grades(grades_info)
