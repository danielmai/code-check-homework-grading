#!/usr/bin/env python

"""
open_reports_in_browser

Looks at a submission folder and opens the code-check report.html
files within a certain name range (or all of them) in the default
application.
"""

import argparse
import os
import re
import webbrowser
import sys
import time

try:
    # For Python 3
    import urllib.parse as urllibparse
    import urllib.request as urllibrequest
except ImportError:
    # For Python 2.7.*
    import urlparse as urllibparse
    import urllib as urllibrequest

def path2url(path):
    return urllibparse.urljoin('file:', urllibrequest.pathname2url(path))

parser = argparse.ArgumentParser()
parser.add_argument('directory', help='the submission directory.')
parser.add_argument('-j', '--java-files',
                    help='open aggregate java files.', action='store_true')
parser.add_argument('-s', '--start-letter',
                    help='the last name letter to start opening reports for')
parser.add_argument('-e', '--end-letter',
                    help='the last name letter to stop opening reports for')
parser.add_argument('-se', '--start-and-end',
                    help=('the last name letter to open reports for '
                          '(start + end functionality)'))
parser.add_argument('-p', '--progress',
                    help='show grading progress (i.e. how many file left to grade assuming you grade in alphabetical order)',
                    action='store_true')

args = parser.parse_args()

sl = el = ''
if args.start_and_end:
    args.start_letter = args.end_letter = args.start_and_end

if args.start_letter:
    if len(args.start_letter) > 1:
        print('Key invalid. Should be one letter.')
        sys.exit(1)
    sl = args.start_letter.lower()
if args.end_letter:
    if len(args.end_letter) > 1:
        print('Key invalid. Should be one letter.')
        sys.exit(1)
    el = args.end_letter.lower()

if sl and not el:
    el = 'z'
elif el and not sl:
    sl = 'a'
if not sl and not el: # no parameters given
    sl = 'a'
    el = 'z'

print(('Opening student reports starting '
       'with "%s" last names until "%s" last names') % (sl, el))
print('The reports for the following students are opened...')

valid_pattern = '^[%s-%s]' % (sl, el) # We'll look for directories that start with this pattern
files = os.listdir(args.directory) # Returns all the files/folders in the given directory
if "grades.txt" in files: # We don't care to keep track of this file
	files.remove("grades.txt")
files.sort() # Alphabetically sort our list of files
if not (args.directory.endswith("/")): # Make sure our directory string ends with "/"
    args.directory += "/"

for file in files:
    match = re.search(valid_pattern, file)
    if match and os.path.isdir(args.directory + file):
        # True if file is a directory and matches our valid_pattern
        path_to_file = args.directory + file + "/"
        # Now we append the desired file to our path, by default .html, if -j specified then .txt.
        if args.java_files:
            path_to_file += "aggregate_java_submissions.txt"
        else:
            path_to_file += "aggregate_report.html"
        # Now open our file
        webbrowser.open(path_to_file)
        print(file)
        # Specify a delay before opening the next tab, that way they will open up in the browser
        # still in the correct order (i.e. without the delay, files would get put in the wrong order).
        # This could probably be optimized...
#        time.sleep(.5)

if args.progress:
    # If -p is passed on command line: gather some stats and print them out
    count_opened = 0
    count_already_done = 0
    count_not_done = 0
    for file in files:
        match = re.search(valid_pattern, file)
        if match and os.path.isdir(args.directory + file):
            count_opened += 1
        elif count_opened == 0:
            count_already_done += 1
        else:
            count_not_done += 1
    print('Opened ' + str(count_opened) + " files. " + str(count_not_done) + " files left. " + str(count_already_done) + " files aleady done.")


print('Done opening reports.')
