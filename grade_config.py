#!/usr/bin/env python

"""
grade_config

Configures config.json for information on this course.
"""

import argparse
import json
import os
import sys

# Python 2 compatibility
if sys.version[0] == "2": input = raw_input

def set_host():
    host = input("Change host to: ")
    config_data['host'] = host

def set_access_token():
    access_token = input("Change access token to: ")
    config_data['access_token'] = access_token

def set_course_name():
    course_name = input("Change course name to: ")
    config_data['course_name'] = course_name

def set_course_id():
    course_id = input("Change course id to: ")
    config_data['course_id'] = course_id

def script_path():
    """Returns the absolute path of this script"""
    pathname = os.path.dirname(sys.argv[0])
    return os.path.abspath(pathname)

parser = argparse.ArgumentParser(description="Choose the '--all' option to modify all of the fields in config.json. Or choose one or more of the other options to specify the fields that you want to modify.")

parser.add_argument('--all',
                    help="configure all fields.",
                    action="store_true")
parser.add_argument('--host',
                    help=('configure the host. '
                          'e.g., sjsu.instructure.com'),
                    action='store_true')
parser.add_argument('--access-token',
                    help=('configure the API access token. '
                          'The API access token can be '
                          'generated on the Settings page on Canvas.'),
                    action='store_true')
parser.add_argument('--course-id',
                    help=('configure the course id. The course id can be found '
                          'in the URL for the course page on Canvas.'),
                    action='store_true')
parser.add_argument('--course-name',
                    help=('configure the course name. '
                          'This parameter is optional, '
                          'but it acts a label for the associated course id.'),
                    action='store_true')
args = parser.parse_args()

config_filepath = '%s/config.json' % script_path()

config_data = {}
with open(config_filepath, 'r') as f:
    config_data = json.loads(f.read())

if args.all:
    set_host()
    set_access_token()
    set_course_name()
    set_course_id()
else:
    if args.host:
        set_host()
    if args.access_token:
        set_access_token()
    if args.course_name:
        set_course_name()
    if args.course_id:
        set_course_id()

with open(config_filepath, 'w') as f:
    json_data = json.dumps(config_data, sort_keys=True,
                           indent=4, separators=(',' ,': '))
    f.write(json_data)

with open(config_filepath, 'r') as f:
    print("config.json:")
    print(f.read())
