#!/usr/bin/env python

"""
grade

The main script. It organizes submissions downloaded from Canvas,
gets the code-check scores, weighs the scores accordingly, and
provides a text file (grades.txt) for the grader to use for grading.
"""
from __future__ import print_function
import argparse
import codecs
import json
import logging
import os
import re
import shutil
import subprocess
import sys
from collections import OrderedDict
from collections import Counter
from collections import defaultdict
from pprint import pprint

from course import Course
from courserequests import CourseRequests

# Create logger
log = logging.getLogger(__name__)

# Python 2 compatibility
if sys.version[0] == "2": input = raw_input
else: unicode = str

# String character encoding
encoding = "UTF-8"

# grades.txt keys
ASSIGNMENT_NAME_KEY = "_canvas_assignment_name" # The Canvas assignment name
ASSIGNMENT_ID_KEY = "_canvas_assignment_id" # the Canvas assignment id

DIR_KEY = "_dir" # the name of the directory with the student's submission
NAME_KEY = "_name" # the name of the student
ID_KEY = "_canvas_id" # the Canvas ID of the student
SCORE_KEY = "_total_score" # the score the student received for the assignment
COMMENT_KEY = "_comments" # comments for a student's submission on Canvas and score modifications
EXTRA_KEY = "_notes" # notes and score modifications

SEPARATOR = '-' * 8

# flag notes to print at the end of the script,
# such as students who submitted unverified jars
# key: Canvas ID -> value: Comment/Extra
comment_notes = {}
extra_notes = {}

# Canvas IDs that received full points
perfect_score_ids = []

# Counter for each submission
# (Duplicate submissions among students (i.e., cheating) will result in
# a count higher than 1)
submission_id_counts = Counter()

# Counter for the problem ids of the submissions
problem_id_counts = Counter()

# Counter for the problem levels of the submissions
# No longer used since codecheck doesn't supply problem levels anymore
#problem_level_counts = Counter()

# the list of submission filenames that weren't signed
unsigned_submissions = []

# Mapping from Canvas user IDs to names
canvasIdsToNames = {}

def check_num_parts():
    """
    Checks whether the grader has provided the number of parts
    for the assignment. If not, the grader is prompted for the
    number of parts.

    A valid number of parts is a non-negative integer.
    """

    err_msg = "That's not a valid number of parts."
    if not args.number_parts:
        while True:
            num_parts = int(input('> How many parts are in %s? ' % (
                    args.assignment_name)))
            if num_parts > 0:
                args.number_parts = num_parts
                break
            else:
                log.warning(err_msg)
    elif args.number_parts < 1:
        log.critical(err_msg + " Quitting program.")
        sys.exit(1)

def count_codecheck_metadata(directory):
    """Gets the counter objects with the counts for the submitted
    problem ids and the problem levels (draft or final) for the
    report.html files walking down the directory."""
    submission_ids = []
    problem_ids = []
    #problem_levels = [] # 1 for draft; 2 for final

    sub_pat = r'<meta name="Submission" content="/tmp/codecheck/(\w+)"/>'
    prob_pat = r'<meta name="Problem" content="([\w/]+)"/>'
    lvl_pat = r'<meta name="Level" content="(\d)"/>'

    def accumulate_id(report_text, regex_pat, accumulating_list):
        """Append the matching regex group from the report text
        to the accumulating_list"""
        match = re.search(regex_pat, report_text)
        if match:
            found_id = match.group(1)
            accumulating_list.append(found_id)
        else:
            log.error("'%s' not found in report text", regex_pat)
            return None

    cwd_before_method = os.getcwd()
    os.chdir(directory)

    last_folder = ""
    last_r = ""

    for dir_name, subdir_list, file_list in os.walk(directory):
        if 'report.html' in file_list:
            with codecs.open('%s/report.html' % dir_name, 'r', "utf-8") as f:
                r = f.read()

                if (r == last_r) and (dir_name.split(os.sep)[-2] == last_folder):
                        continue

                last_folder = dir_name.split(os.sep)[-2]
                last_r = r

                accumulate_id(r, sub_pat, submission_ids)
                accumulate_id(r, prob_pat, problem_ids)
                # accumulate_id(r, lvl_pat, problem_levels)

    os.chdir(cwd_before_method)
    global submission_id_counts, problem_id_counts #, problem_level_counts
    submission_id_counts = Counter(submission_ids)
    problem_id_counts = Counter(problem_ids)
    # problem_level_counts = Counter(problem_levels)

def organize_files_to_folders_per_student(directory):
    """"Separates submissions into directories per student"""
    def get_student_directory_name(s):
        """Gets the student directory name (containing the name and Canvas ID)
        from the string s"""
        MIN_NUMBER_OF_DIGITS = 5
        cid_pattern = "(^[a-zA-Z_.-]+(\d{%d,}))" % MIN_NUMBER_OF_DIGITS
        match = re.search(cid_pattern, s)
        if match:
            # Getting CanvasID from filename
            split_file_name = s.split("_")

            # Getting name from CanvasID
            if (split_file_name[1].lower() == "late"):
                student_name_from_id = canvasIdsToNames.get(int(split_file_name[2]))
            else:
                student_name_from_id = canvasIdsToNames.get(int(split_file_name[1]))

            if student_name_from_id is None:
                log.info("Error: Student id was not found in the selected course! Please check the config file.")
                sys.exit()

            # Building format: firstnames--lastnames-late(if late)_CanvasID
            student_name_from_id = student_name_from_id.replace(" ", "")
            student_name_from_id = student_name_from_id.split(",")
            student_name_from_id = student_name_from_id[0] + "--" + student_name_from_id[1][0:]

            if (split_file_name[1].lower() == "late"):
                student_dir = student_name_from_id + "-" + split_file_name[1].lower() + "_" + split_file_name[2]
            else:
                student_dir = student_name_from_id + "_" + split_file_name[1]

            return student_dir.lower()

    os.chdir(directory)
    student_dirs = []

    for dir_name, subdir_list, file_list in os.walk(unicode(directory)):
        for file_name in file_list:

            log.debug('file_name = %s', file_name)

            student_dir = get_student_directory_name(file_name)
            if student_dir:
                if student_dir not in student_dirs:
                    log.debug("%s does not exist. Creating directory.",
                              student_dir)
                    try:
                        os.mkdir(student_dir)
                    except OSError:
                        sys.exit("Error: This script should be run on a fresh set of submissions.")

                log.debug("Moving " + file_name + " to the student directory")

                shutil.move(file_name, student_dir)
                student_dirs.append(student_dir)


def extract_jars(directory):
    """Extracts all the jars in the directory using
    `jar xvf ...`"""
    def verify_jar(jar_file):
        """
        Verify jar_file.

        Returns True if jar_file is signed correctly; False otherwise.
        """
        command = ['jarsigner',
                   '-keystore', '%s/%s' % (sp, 'codecheck-public.jks'),
                   '-storepass', 'secret',
                   '-verify', jar_file]
        proc = subprocess.Popen(command,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)


        jarsigner_output = proc.communicate()
        return 'jar verified' in jarsigner_output[0].decode(encoding)

    def strip_extension(filename, possible_exts):
        """Removes the extension from the filename"""
        for ext in possible_exts:
            if filename.endswith(ext):
                return filename[:-len(ext)]

    def extract_jar(jar_file_path, verbose=False):
        """Extracts the jar file indicated by the path."""
        jar_args = 'xvf' if verbose else 'xf'
        subprocess.call(['jar', jar_args, jar_file_path])

    cwd_before_method = os.getcwd()
    os.chdir(directory)

    ext = ('.jar', '.signed.zip', '.zip')
    for dir_name, subdir_list, file_list in os.walk(directory):
        for file_name in file_list:

            if file_name.endswith(ext):
                os.chdir(dir_name)
                file_name_no_ext = strip_extension(file_name, ext)
                if not os.path.exists(file_name_no_ext):
                    if args.no_verify:
                        os.mkdir(file_name_no_ext)
                        os.chdir(file_name_no_ext)
                        jar_file_path = "../%s" % file_name
                        log.info('Extracting jar %s', file_name)
                        extract_jar(jar_file_path, verbose=args.verbose)
                    elif verify_jar(file_name):
                        os.mkdir(file_name_no_ext)
                        os.chdir(file_name_no_ext)
                        jar_file_path = "../%s" % file_name
                        log.debug('Extracting verified jar %s', file_name)
                        extract_jar(jar_file_path, verbose=args.verbose)
                    else:
                        log.warning("**Warning**: %s is unverified.", file_name)
                        unsigned_submissions.append(file_name)
                    os.chdir('..')
                else:
                    sys.exit(('Extraction process has been done already. '
                              'Exiting program.'))



    os.chdir(cwd_before_method)

def get_canvas_id(s):
    """Gets the Canvas ID from the string s"""
    MIN_NUMBER_OF_DIGITS = 5
    cid_pattern = "(^[a-zA-Z_.-]+(\d{%d,}))" % MIN_NUMBER_OF_DIGITS
    match = re.search(cid_pattern, s)
    if match:
        return match.group(2)
    else:
        log.error("**Error**: Can't extract the Canvas ID from %s", s)

def get_student_name(canvas_id):
    return canvasIdsToNames.get(canvas_id)

def create_totals_per_student(directory):
    """
    Creates the score totals for each student in the directory.
    """

    def extract_re_group(regex_pat, text):
        """Extracts the first _captured_ group from the regex_pat from text"""
        match = re.search(regex_pat, text)
        if match:
            return match.group(1)

    def extract_submission_id_from_report(report):
        """Extracts the submission ID from a codecheck report file read
        as a string."""
        return extract_re_group(r'<meta name="Submission" content="/tmp/codecheck/(\w+)"/>', report) # Bug was fixed over here. The old script had "/submission" which is not the case in report.html

    def extract_problem_id_from_report(report):
        """Extracts the problem ID from a codecheck report file read
        as a string."""
        return extract_re_group(r'<meta name="Problem" content="([\w/]+)"/>', report)

    # This function is deprecated since Codecheck no longer uses levels in its formatting.
    # Thus, level checking code is still in this script, but is no longer used.
    def extract_problem_level_from_report(report):
        """Extracts the problem level from a codecheck report file read
        as a string."""
        return extract_re_group(r'<meta name="Level" content="(\d)"/>', report)

    def extract_classname_from_report(report):
        """Extracts the Java class name from a codecheck report file read
        as a string."""
        pattern = (r'<div class="studentFiles">\s*'
                   r'<p class="caption">([a-zA-Z0-9_.]+):</p>')
        return extract_re_group(pattern, report)

    def extract_score_from_report(report):
        """Extracts the score from a codecheck report file read
        as a string."""
        report_lines = report.splitlines()
        report_lines.reverse() # reverse because the score is near the end of
                               # the report

        score_key_found = False;
        score_index = 0
        score_key = r'<p class="score">'

        for line in report_lines:
            if re.search(score_key, line):
                score_key_found = True;
                break
            score_index += 1

        score_pattern = r'((\d+/\d+)|(\d))'
        match = re.search(score_pattern, report_lines[score_index])
        if match:
            return match.group(1)
        else:
            log.info("ERROR: Score pattern was not found!")

    def get_part_with_warnings(part, student_id, classnames, assignment):
        """Checks for...
        cheating - if the sub_id is a duplicate
        correctness - prob id is part of the prob_ids list
                      prob_lvl is correct
        """

        # If the student has submitted the same file multiple times, don't count
        # the duplicates. Only count one of them.
        if part['classname'] in classnames['student_id']:
            part['score'] = '0'
            part['reasons'].append('Already submitted.')

        # Codecheck level no longer used by codecheck
        #if assignment['codecheck_level']:
        #    if part['problem_level'] != assignment['codecheck_level']:
        #        part['score'] = '0'
        #        part['reasons'].append("Incorrect problem level")

        if part['problem_id'] not in assignment['codecheck_problem_ids']:
            part['score'] = '0'
            part['reasons'].append("%s not part of this assignment" % (
                                    part['classname']))

        if part['submission_id'] in assignment['duplicate_ids']:
            part['score'] = '0'
            part['reasons'].append(("This exact codecheck submission has been "
                                    "turned in by another student. Submission "
                                    "ID = {}").format(part['submission_id']))
        return part

    def write_total_per_part():
        """Writes the total score extracted per 'report.html'
        for each part in one 'total_grade.txt' per student."""
        cwd_before_method = os.getcwd()
        os.chdir(directory)

        last_student_id = ""
        last_r = ""

        classnames_per_student = defaultdict(set)
        for dir_name, subdir_list, file_list in os.walk(directory):
            if 'report.html' in file_list:
                os.chdir(dir_name)
                student_dir_name = os.getcwd().split(os.sep)[-2]
                student_id = get_canvas_id(student_dir_name)

                with codecs.open('report.html', 'r', 'utf-8') as f:
                    r = f.read()
                    if (r == last_r) and (student_id == last_student_id):
                        continue

                    part = {
                        'score': extract_score_from_report(r),
                        'classname': extract_classname_from_report(r),
                        'submission_id': extract_submission_id_from_report(r),
                        'problem_id': extract_problem_id_from_report(r),
                        #'codecheck_problem_ids' : extract_problem_level_from_report(r),
                        'reasons': []
                    }

                    last_student_id = student_id
                    last_r = r


                if not args.no_checks:
                    global assignment
                    part = get_part_with_warnings(part,
                                                  student_id,
                                                  classnames_per_student,
                                                  assignment)

                classnames_per_student[student_id].add(part['classname'])

                with codecs.open('../total_grade.txt', 'a', 'utf-8') as grade_file:
                    strformat = (part['classname'], part['score'])
                    if part['reasons']:
                        msg = '%s %s :: %s\n'
                        reasons = '; '.join(part['reasons'])
                        strformat = strformat + (reasons,)
                    else:
                        msg = '%s %s\n'
                    grade_file.write(msg % strformat)


        os.chdir(cwd_before_method)

    def write_total_per_assignment():
        """Writes grade totals in 'total_grade.txt' file per student
        Precondition: The directory hirearchy corresponds with the
        codecheck structure.
        """
        cwd_before_method = os.getcwd()
        os.chdir(directory)

        for dir_name, subdir_list, file_list in os.walk(directory):
            os.chdir(dir_name)
            if 'total_grade.txt' in file_list:
                with codecs.open('total_grade.txt', 'r+', 'utf-8') as f:
                    total_points = 0

                    for line in f:
                        match = re.search(r'(\d+)/(\d+)', line)
                        if match:
                            numerator = match.group(1)
                            denominator = match.group(2)
                            total_points += float(numerator) / int(denominator)

                    unverified_assignments = 0
                    unverified_assignment_names = [];
                    student_name_id = subdir_list[0].split('_')[0] + subdir_list[0].split('_')[1]

                    for submission in unsigned_submissions:
                        submission_name_id = submission.split('_')[0] + submission.split('_')[1]
                        if student_name_id == submission_name_id:
                            unverified_assignments += 1
                            unverified_assignment_names.append(submission)

                    while unverified_assignments > 0:
                        f.write('Submission 0 ::  '+ unverified_assignment_names[unverified_assignments-1] +' unverified submission\n')
                        unverified_assignments -= 1

                    total_numerator = int(assignment_max_score \
                        * total_points / args.number_parts)
                    f.write('\n%s: %d/%d' % (SCORE_KEY, total_numerator, assignment_max_score))

                    if total_numerator == assignment_max_score:
                        current_dir = dir_name.split(os.path.sep)[-1]
                        perfect_score_ids.append(get_canvas_id(current_dir))

        os.chdir(cwd_before_method)

    write_total_per_part()
    write_total_per_assignment()

def write_aggregate_report_files_per_student(directory):
    """
    Writes the report files into one `aggregate_report.html` file for
    each student directory.
    Precondition: The directory hirearchy corresponds with the
    codecheck structure.
    """
    cwd_before_method = os.getcwd()
    os.chdir(directory)

    for dir_name, subdir_list, file_list in os.walk(directory):
        if 'report.html' in file_list:
            os.chdir(dir_name)
            with codecs.open('report.html', 'r', 'utf-8') as f:
                r = f.read()
                with codecs.open('../aggregate_report.html', 'a', 'utf-8') as agg_report:
                    agg_report.write(r)
                    report_sep = '<div style="border:2px dashed #aaa;"></div>'
                    agg_report.write('\n{}\n'.format(report_sep))

    os.chdir(cwd_before_method)

def write_aggregate_java_submissions_per_student(directory):
    """Writes the java files into one `aggregate_java_submissions.txt`
    for each student directory.
    Precondition: The directory hirearchy corresponds with the
    codecheck structure.
    """
    cwd_before_method = os.getcwd()
    os.chdir(directory)

    NUM_SEPARATE = 50
    submission_separator = '/*' +  ('*' * NUM_SEPARATE) + '*/'

    for dir_name, subdir_list, file_list in os.walk(directory):
        for filename in file_list:
            if filename.endswith('java'):
                os.chdir(dir_name)
                f = codecs.open(filename, 'r')
                jfile = codecs.open('../aggregate_java_submissions.txt', 'a')

                submission_code = f.read()
                jfile.write(submission_code)
                jfile.write('\n{}\n'.format(submission_separator))

                f.close()
                jfile.close()

    os.chdir(cwd_before_method)

def remove_total_grade_files(directory):
    """
    Removes all grade-related files in `directory`
    That includes
      - grades.txt
      - total_grade.txt
      - aggregate_java_submissions.txt
      - aggregate_report.html
    """
    def remove(filename):
        """Deletes the file with the filename"""
        subprocess.call(['rm', filename])

    cwd_before_method = os.getcwd()
    os.chdir(directory)
    if 'grades.txt' in os.listdir(directory):
        remove('grades.txt')
    for dir_name, subdir_list, file_list in os.walk(directory):
        os.chdir(dir_name)
        if 'total_grade.txt' in file_list:
            remove('total_grade.txt')
        if 'aggregate_java_submissions.txt' in file_list:
            remove('aggregate_java_submissions.txt')
        if 'aggregate_report.html' in file_list:
            remove('aggregate_report.html')

    os.chdir(cwd_before_method)

def remove_extracted_jar_dirs(directory):
    """Removes all the directories of the extracted jar files"""
    cwd_before_method = os.getcwd()
    os.chdir(directory)

    log.debug("Removing extracted directories...")
    for item in os.listdir(directory):
        if os.path.isdir(item):
            os.chdir(item)
            for sub_item in os.listdir('.'):
                if os.path.isdir(sub_item):
                    subprocess.call(['rm', '-rf', sub_item])
            os.chdir('..')


    os.chdir(cwd_before_method)

def write_aggregate_grades_comments_and_extras(directory):
    """Aggregates all of the 'total_grade.txt' files in each student
    directory into a single 'grades.txt' file with 'comment' and 'extra'
    fields to fill in."""

    def write_assignment_info_header(assignment_name, assignment_id):
        """Writes the assignment name and id to the top of
        the grades.txt file"""
        with codecs.open('%s/grades.txt' % directory, 'a', 'utf-8') as grades_file:
            fields = OrderedDict([(ASSIGNMENT_NAME_KEY, assignment_name),
                                  (ASSIGNMENT_ID_KEY, assignment_id)])
            for key, value in fields.items():
                grades_file.write('{}: {}\n'.format(key, value))
            grades_file.write('{}\n'.format(SEPARATOR))

    def get_part_problems(total_grade_text):
        """Gets any problems from total_grades.txt and formats it for
        writing to the EXTRA_KEY in grades.txt"""
        problems = ''
        for line in total_grade_text.splitlines():
            split = line.split(' :: ')
            if len(split) > 1:  # if there are any problems
                for problem_desc in split[1].split('; '):
                    prob_msg = "{classname} - {description} ({pts} pts)))\n"

                    # deduct points
                    points = assignment['points_possible']
                    weighted_points = int(-points // args.number_parts)
                    prob_msg = prob_msg.format(classname=split[0].split()[0],
                                               description=problem_desc,
                                               pts=weighted_points)
                    problems += prob_msg
        return problems


    write_assignment_info_header(assignment['name'], assignment['id'])
    cwd_before_method = os.getcwd()
    os.chdir(directory)
    NUM_BLANK_LINES = 5
    for item in sorted(os.listdir(directory)):
        if os.path.isdir(item):
            os.chdir(item)
            score = '0/%s' % assignment_max_score # default to zero-score
            problems = None
            if 'total_grade.txt' in os.listdir('.'):
                log.debug('found total_grade.txt in %s', item)
                with codecs.open('total_grade.txt', 'r', 'utf-8') as grade_file:
                    content = grade_file.read()
                    problems = get_part_problems(content)
                    score = content.splitlines()[-1].split()[-1]
            with codecs.open('%s/grades.txt' % directory, 'a', 'utf-8') as grades_file:
                name_and_id = item
                canvas_id = get_canvas_id(name_and_id)
                name = get_student_name(int(canvas_id))
                codecheck_points_comment = "\nCodecheck score: " + score;
                if not name:
                    name = "See {} for name. Could not find name in Canvas API for ID '{}'.".format(DIR_KEY, canvas_id)

                if name and canvas_id:
                    entries = OrderedDict(
                        [(DIR_KEY, item),
                         (NAME_KEY, name),
                         (ID_KEY, canvas_id),
                         (SCORE_KEY, score),
                         (COMMENT_KEY, codecheck_points_comment + '\n' * NUM_BLANK_LINES),
                         (EXTRA_KEY, '\n' * NUM_BLANK_LINES)]
                    )

                    if problems:
                        message = "\nPoints already deducted for following:\n"
                        message = message + problems
                        entries[EXTRA_KEY] = message + entries[EXTRA_KEY]

                    for key, value in entries.items():
                        grades_file.write("{}: {}\n".format(key, value))
                    grades_file.write((SEPARATOR) + '\n')
        os.chdir(directory)

    os.chdir(cwd_before_method)

def script_path():
    """Returns the absolute path of this script"""
    pathname = os.path.dirname(sys.argv[0])
    return os.path.abspath(pathname)

def parse_args():
    """Create and return the argument parser."""
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose',
                        help="print verbose output to stdout.",
                        action='store_true')
    parser.add_argument('-n', '--number-parts',
                        help='provide the number of parts for this assignment',
                        type=int)
    parser.add_argument('--no-verify',
                        help=('skip jarsigner verification and unarchive '
                              'all submission files'),
                        action='store_true')
    parser.add_argument('--no-checks',
                        help=("Don't check for warnings/cheating submissions "
                              "(checks by default)"),
                        action="store_true")
    parser.add_argument('--no-organize',
                        help=("If the files have already been organized, "
                        "do not organize the files again"),
                        action="store_true")
    parser.add_argument('--no-extract',
                        help=("If the files have already been extracted, "
                              "do not extract the files again"),
                        action="store_true")
    parser.add_argument('-r', '--remove-existing',
                        help=('removes grading files from the directory '
                              'from a previous run'),
                        action='store_true')
    parser.add_argument('assignment_name',
                        help=('The assignment name. '
                              'Must match an assignment found on Canvas.'))
    parser.add_argument('directory',
                        help=('the directory containing subdirectories of '
                              'student submissions.'))
    return parser.parse_args()

def init_logging(verbose):
    """Initialize logging."""
    global log

    if verbose:
        loglevel = logging.DEBUG
        logfmt = '%(levelname)-8s [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s'
    else:
        loglevel = logging.INFO
        logfmt = '%(message)s'

    logging.basicConfig(format=logfmt, level=loglevel)

if __name__ == '__main__':
    args = parse_args()
    init_logging(args.verbose)
    args.directory = os.path.abspath(args.directory)

    # We're going to need the script's path later
    sp = script_path()


    # Canvas config info
    canvas_keys = {}
    with codecs.open('%s/config.json' % script_path(), 'r', 'utf-8') as config_file:
        canvas_keys = json.loads(config_file.read())

    course = Course(canvas_keys['course_name'], canvas_keys['course_id'])
    cr = CourseRequests(course, canvas_keys['host'],
                        canvas_keys['access_token'])

    # Checking if access is granted
    catching_error = cr.list_all_assignments()
    if 'errors' in catching_error:
        log.info("Error while accessing canvas: " + catching_error['errors'][0]['message'])
        sys.exit()



    # This runs if you (the grader) want to remove the files
    if args.remove_existing:
        response = input('Do you want to REMOVE the following files'
                          '(if they exist)?'
                          '\n'
                          '- grades.txt\n'
                          '- total_grade.txt\n'
                          '- aggregate_java_submissions.txt\n'
                          '- aggregate_report.html\n'
                          '(y/n)  ')
        if response.lower() == 'y':
            log.info("Removing `total_grade.txt`, `grades.txt`, "
                     "and aggregate files...")
            remove_total_grade_files(args.directory)

    log.info("Getting assignment information from Canvas...")
    assignment = next((a
                       for a in cr.list_all_assignments()
                       if a['name'] == args.assignment_name), None)
    if assignment is None:
        sys.exit(("**Error.**\n"
                  "The assignment '%s' does not exist on Canvas. "
                  "Make sure that the assignment name matches "
                  "EXACTLY as it does on Canvas.") % args.assignment_name)

    print("Testing assignment name: " + args.assignment_name)

    assignment_max_score = int(assignment['points_possible'])

    log.info("The max score for %s is %d point(s).", args.assignment_name,
             assignment_max_score)

    # Get canvas user ids to names
    all_enrollments = cr.list_all_enrollments()
    for enrollment in all_enrollments:
        user_canvas_id = enrollment['user_id']
        user_name = enrollment['user']['sortable_name']
        canvasIdsToNames[user_canvas_id] = user_name
    log.debug(canvasIdsToNames)

    check_num_parts()

    if not args.no_organize:
        log.info("Organizing files...")
        organize_files_to_folders_per_student(args.directory)

    if not args.no_extract:
        log.info("Extracting jars...")
        extract_jars(args.directory)

    if not args.no_checks:
        log.info('Determining codecheck problem level (draft/final) and IDs...')
        count_codecheck_metadata(args.directory)

        # This commented code used to always run. Canvas has since changed the formatting
        # of the submissions, which means we no longer use the problem level here.
        #log.info("Number of draft-level submissions: %d", problem_level_counts['1'])
        #log.info("Number of final-level submissions: %d", problem_level_counts['2'])
        #try:
            #assignment['codecheck_level'] = problem_level_counts.most_common()[0][0]
        #except IndexError:
            #assignment['codecheck_level'] = ''

        # 10 is magic. I'm just guessing that it'll take at least 10 students
        # to submit the same thing for it to actually count as a valid
        # question that was part of the assignment
        assignment['codecheck_problem_ids'] = \
          [x[0] for x in problem_id_counts.most_common() if x[1] > 10]
        assignment['duplicate_ids'] = [i for i,count in submission_id_counts.items() if count > 1]

        log.info("The problem ids accepted for this assignment:")
        pprint(assignment['codecheck_problem_ids'])
        log.info("The submission ids found to be duplicates:")
        if assignment['duplicate_ids']:
            for duplicate_id in assignment['duplicate_ids']:
                print(duplicate_id)
        else:
            log.info(None)

    log.info("Creating totals for each student...")
    create_totals_per_student(args.directory)

    log.info('Aggregating java submissions to '
             '"aggregate_java_submissions.txt" in each student directory...')
    write_aggregate_java_submissions_per_student(args.directory)

    log.info('Aggregating report.html files to '
             '"aggregate_report.html" in each student directory...')
    write_aggregate_report_files_per_student(args.directory)

    log.info("Aggregating all grades into grades.txt...")
    write_aggregate_grades_comments_and_extras(args.directory)

    log.info("Done. Now open 'grades.txt' "
             "in the submissions directory to grade submissions.")
